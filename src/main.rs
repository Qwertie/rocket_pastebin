#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;

extern crate rocket_pastebin;
extern crate diesel;

use self::rocket_pastebin::*;
use self::models::*;
use self::diesel::prelude::*;

fn main() {
    rocket::ignite()
        .manage(establish_connection())
        .launch();
}

