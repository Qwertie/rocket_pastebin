#[derive(Queryable)]

pub struct Paste {
    pub id: i32,
    pub body: String,
}
